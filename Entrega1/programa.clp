(deftemplate jarra
    (slot cervezas
        (type INTEGER)
        (default 0)
    )
)

(defrule followTheRule
    =>
    (assert (jarra))
)

(defrule masUno
    ?jarra <- (jarra (cervezas ?cervezas))
    =>
    (modify ?jarra(cervezas (+ 1 ?cervezas)))
)

(defrule masDos
    ?jarra <- (jarra (cervezas ?cervezas))
    =>
    (modify ?jarra(cervezas (+ 2 ?cervezas)))
)

(defrule paCasa
    (declare (salience 10000)) ;Prioridad maximun over 9000
    ?jarra <- (jarra (cervezas 3)) ;Si hay una jarra con 3 litros de zumo
    =>
    (printout t "LA T imprime por PAnTaLLa" crlf) ;Imprime cosas
    (retract ?jarra) ;Elimina la jarra del dominio
)