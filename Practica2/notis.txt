./ff -o domains/emociones/domain.pddl -f domains/emociones/problema-1.pddl


Problema 1 - 1 escenario, 1 accion
	El problema tendrá una única acción pero con distintos niveles de moralidad y con distintos rasgos de la personalidad para poder escogerse. Se debería ejecutar la mejor acción que se pueda ejecutar.

Problema 2 - 1 escenario, varias acciones requisitos de personalidad profundos
	Se comprobará que sólo se escogen las acciones que se pueden ejecutar

Problema 3 - 1 escenario , varias acción por escenario, no se cumplen los requisitos de personalidad para realizar todas las acciones
	El problema no se solucionará

Problema 4 - 3 escenarios, 1 accion por escenario
	Se comprobará que cambia de escenario

Problema 5 - 3 escenarios, varias acciones por escenario
	Prueba completa

Problema 6 - 1 escenarios, varias acciones por escenario, 3 bots
	Se comprobará como se adapta el problema con 3 bots en un escenario con varias acciones ciclicas




cambiar escenario ha cambiado