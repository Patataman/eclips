
ff: parsing domain file
domain 'TALKING' defined
 ... done.
ff: parsing problem file
problem 'SIX' defined
 ... done.


metric established (normalized to minimize): ((1.00*[RF1](PUNTOS)) - () + 0.00)

checking for cyclic := effects --- OK.

ff: search configuration is  best-first on 1*g(s) + 5*h(s) where
    metric is ((1.00*[RF1](PUNTOS)) - () + 0.00)

advancing to distance:   16
                         15
                         14
                         13
                         12
                         11
                         10
                          9
                          8
                          7
                          6
                          5
                          4
                          3
                          2
                          1
                          0

FINAL FLUENTS VALUES BF
[RF0](RONDA): 15.00
[RF1](PUNTOS): 47.00
[RF2](MINUS-RONDA): -15.00

ff: found legal plan as follows

step    0: GESTIONAR JUAN TINO DEBATE NEUTRAL AGRESIVIDAD NEUTRAL DECIR-ALGO-MALO MALA ABIERTA TOKEN
        1: GESTIONAR TINO JUAN DEBATE AGRESIVIDAD AGRESIVIDAD NEUTRAL DECIR-ALGO-MALOAA MALA INESTABLE TOKEN
        2: GESTIONAR JUAN TINO DEBATE AGRESIVIDAD CONFIANZA AGRESIVIDAD DECIR-ALGO-BUENO BUENA ABIERTA TOKEN
        3: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION NEUTRAL EXPRESAR-OPINION REGULAR INESTABLE TOKEN
        4: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN
        5: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN
        6: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN
        7: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN
        8: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN
        9: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN
       10: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN
       11: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN
       12: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN
       13: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN
       14: GESTIONAR ISABEL JUAN DEBATE INDIGNACION CONFIANZA AGRESIVIDAD DAR-RAZONI REGULAR RESPONSABLE TOKEN
       15: FINALIZARESCENARIO TINO ISABEL DEBATE
     

time spent:    0.22 seconds instantiating 3614 easy, 0 hard action templates
               0.87 seconds reachability analysis, yielding 20 facts and 230 actions
               0.00 seconds creating final representation with 20 relevant facts, 3 relevant fluents
               0.01 seconds computing LNF
               0.00 seconds building connectivity graph
               0.00 seconds searching, evaluating 48 states, to a max depth of 0
               1.10 seconds total time

