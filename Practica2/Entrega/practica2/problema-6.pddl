(define (problem six)
(:domain TALKING)
(:objects
	debate - escenario
	juan - bot
	isabel - bot 
	tino - bot	
	Agresividad - emocion
	Satisfaccion - emocion
	Temor - emocion
	Confianza - emocion
	Paz_Interior - emocion
	Susto - emocion
	Neutral - emocion
	Tristeza - emocion
	Culpabilidad - emocion
	Compasion - emocion
	Alegria - emocion
	Ilusion - emocion
	Placer - emocion
	Amor - emocion
	Enamoramiento - emocion
	Pavor - emocion
	Desilusion - emocion
	Decepcion - emocion
	Indignacion - emocion
	Tranquilidad - emocion
	Agonia - emocion
	Sobresalto - emocion
	Ira- emocion
	abierta - rasgo
	responsable - rasgo
	extrovertida - rasgo
	amable - rasgo
	inestable - rasgo
	Decir-algo-malo - accion
	Decir-algo-maloCA - accion
	Decir-algo-bueno - accion
	Decir-algo-maloCI - accion
	Decir-algo-maloAA - accion
	Expresar-opinion - accion
	Dar-razonI - accion
	Dar-razonA - accion
	Rebatir - accion	
	buena - moral
	regular - moral
	mala - moral
	token - token
)

(:init
	; bots
	(in juan debate)
	(in isabel debate)
	(in tino debate)
	(is juan abierta)
	(is isabel responsable)
	(is tino inestable)
	; emociones iniciales
	(feel juan Neutral)
	(feel isabel Neutral)
	(feel tino Neutral)
	; acciones con los rasgos necesarios (recordar que los resgos necesarios pueden ser varios y conjunciones o disjunciones)
	(accion Decir-algo-malo debate Neutral Agresividad mala)
	(personalidadRequerida Decir-algo-malo abierta)

	(accion Decir-algo-bueno debate Agresividad Confianza buena)
	(personalidadRequerida Decir-algo-bueno abierta)

	(accion Decir-algo-maloAA debate Agresividad Agresividad mala)
	(personalidadRequerida Decir-algo-maloAA inestable)
	(accion Expresar-opinion debate Agresividad Indignacion buena)
	(personalidadRequerida Expresar-opinion inestable)
	(accion Expresar-opinion debate Indignacion Confianza buena)
	(personalidadRequerida Expresar-opinion inestable)
	(accion Expresar-opinion debate Agresividad Indignacion regular)
	(accion Expresar-opinion debate Confianza Indignacion regular)
	(accion Rebatir debate Agresividad Indignacion regular)
	(personalidadRequerida Rebatir responsable)
	(accion Dar-razonI debate Indignacion Confianza regular)
	(accion Dar-razonA debate Agresividad Confianza mala)
	(personalidadRequerida Dar-razonI responsable)
	(personalidadRequerida Dar-razonA abierta)
	(accion Decir-algo-maloCI debate Confianza Indignacion mala)
	(personalidadRequerida Decir-algo-maloCI responsable)
	(accion Decir-algo-maloCA debate Confianza Agresividad mala)
	(personalidadRequerida Decir-algo-maloCA responsable)


	; Primer turno
	(turno juan token)

	(= (puntos) 0)
	(= (puntosMoral buena) 1)
	(= (puntosMoral regular) 3)
	(= (puntosMoral mala) 5)
	(= (ronda) 0)
	(= (minimoRondas debate) 15)
)

(:goal (and
	(escenarioTerminado debate)
))

(:metric minimize (puntos))

)	