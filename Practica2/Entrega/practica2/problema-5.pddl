(define (problem five)
(:domain TALKING)
(:objects
	cita - escenario
	enfado - escenario
	spoiler - escenario
	juan - bot
	isabel - bot 	
	Agresividad - emocion
	Satisfaccion - emocion
	Temor - emocion
	Confianza - emocion
	Paz_Interior - emocion
	Susto - emocion
	Neutral - emocion
	Tristeza - emocion
	Culpabilidad - emocion
	Compasion - emocion
	Alegria - emocion
	Ilusion - emocion
	Placer - emocion
	Amor - emocion
	Enamoramiento - emocion
	Pavor - emocion
	Desilusion - emocion
	Decepcion - emocion
	Indignacion - emocion
	Tranquilidad - emocion
	Agonia - emocion
	Sobresalto - emocion
	Ira- emocion
	abierta - rasgo
	responsable - rasgo
	extrovertida - rasgo
	amable - rasgo
	inestable - rasgo
	Pedir-cita - accion
	Aceptar - accion
	Invitar-al-cine - accion
	Aceptar-propuesta - accion
	Ofrecerse-a-pagar - accion
	Castigar - accion
	Obedecer - accion
	Irse-de-casa - accion
	Escuchar-chapa - accion
	Razonar - accion
	Gritar - accion
	Spoiler - accion
	Devolver-spoiler - accion
	Perdonar - accion
	Decir-que-era-mentira - accion
	Invitar-a-cenar - accion
	Gritar - accion
	buena - moral
	regular - moral
	mala - moral
	token - token
)

(:init
	; bots
	(in juan cita)
	(in isabel cita)
	; personalidades de los bots
	(is juan abierta)
	(is isabel abierta)
	; emociones iniciales
	(feel juan Neutral)
	(feel isabel Neutral)
	; acciones con los rasgos necesarios (recordar que los resgos necesarios pueden ser varios y conjunciones o disjunciones)
	; acciones cita
	(accion Pedir-cita cita Neutral Sobresalto regular)
	(personalidadRequerida Pedir-cita abierta)
	(accion Aceptar cita Sobresalto Ilusion buena)
	(personalidadRequerida Aceptar abierta)
	(accion Invitar-al-cine cita Ilusion Enamoramiento buena)
	(personalidadRequerida Invitar-al-cine abierta)
	(accion Aceptar-propuesta cita Enamoramiento Alegria buena)
	(personalidadRequerida Aceptar-propuesta abierta)
	(accion Ofrecerse-a-pagar cita Alegria Amor buena)
	(personalidadRequerida Ofrecerse-a-pagar abierta)
	; acciones enfado
	(accion Castigar enfado Neutral Temor mala)
	(personalidadRequerida Castigar abierta)
	(accion Obedecer enfado Temor Satisfaccion buena)
	(personalidadRequerida Obedecer abierta)
	(accion Irse-de-casa enfado Temor Susto mala)
	(personalidadRequerida Irse-de-casa abierta)
	(accion Escuchar-chapa enfado Temor Paz_Interior regular)
	(personalidadRequerida Escuchar-chapa abierta)
	(accion Razonar enfado Susto Confianza buena)
	(personalidadRequerida Razonar abierta)
	(accion Gritar enfado Susto Agresividad mala)
	(personalidadRequerida Gritar abierta)
	; acciones spoiler
	(accion Spoiler spoiler Neutral Agresividad mala)
	(personalidadRequerida Spoiler abierta)
	(accion Devolver-spoiler spoiler Agresividad Tristeza mala)
	(personalidadRequerida Devolver-spoiler abierta)
	(accion Perdonar spoiler Agresividad Culpabilidad buena)
	(personalidadRequerida Perdonar abierta)
	(accion Decir-que-era-mentira spoiler Tristeza Culpabilidad regular)
	(personalidadRequerida Decir-que-era-mentira abierta)
	(accion Invitar-a-cenar spoiler Culpabilidad Confianza regular)
	(personalidadRequerida Invitar-a-cenar abierta)
	(accion Gritar spoiler Culpabilidad Susto mala)
	(personalidadRequerida Gritar abierta)


	; Primer turno
	(turno juan token)

	(= (puntos) 0)
	(= (puntosMoral buena) 1)
	(= (puntosMoral regular) 3)
	(= (puntosMoral mala) 5)
	(= (ronda) 0)
	(= (minimoRondas cita ) 5)
	(= (minimoRondas enfado ) 3)
	(= (minimoRondas spoiler ) 4)
)

(:goal (and
	(escenarioTerminado cita)
	(escenarioTerminado enfado)
	(escenarioTerminado spoiler)
))

(:metric minimize (puntos))

)	