(define (problem one)
(:domain TALKING)
(:objects
	cita - escenario
	juan - bot
	isabel - bot 	
	Agresividad - emocion
	Satisfaccion - emocion
	Temor - emocion
	Confianza - emocion
	Paz_Interior - emocion
	Susto - emocion
	Neutral - emocion
	Tristeza - emocion
	Culpabilidad - emocion
	Compasion - emocion
	Alegria - emocion
	Ilusion - emocion
	Placer - emocion
	Amor - emocion
	Enamoramiento - emocion
	Pavor - emocion
	Desilusion - emocion
	Decepcion - emocion
	Indignacion - emocion
	Tranquilidad - emocion
	Agonia - emocion
	Sobresalto - emocion
	Ira- emocion
	abierta - rasgo
	responsable - rasgo
	extrovertida - rasgo
	amable - rasgo
	inestable - rasgo
	Pedir-cita - accion
	buena - moral
	regular - moral
	mala - moral
	token - token
)

(:init
	; bots
	(in juan cita)
	(in isabel cita)
	(is juan abierta)
	(is isabel abierta)
	; emociones iniciales
	(feel juan Neutral)
	(feel isabel Neutral)
	; acciones con los rasgos necesarios (recordar que los rasgos necesarios pueden ser varios y bastará con cumplir uno de ellos.
	(accion Pedir-cita cita Neutral Sobresalto regular)
	(personalidadRequerida Pedir-cita abierta)
	(accion Pedir-cita cita Neutral Sobresalto buena)
	(personalidadRequerida Pedir-cita abierta)
	(accion Pedir-cita cita Neutral Sobresalto mala)
	(personalidadRequerida Pedir-cita abierta)
	(accion Pedir-cita cita Neutral Sobresalto regular)
	(personalidadRequerida Pedir-cita responsable)
	(accion Pedir-cita cita Neutral Sobresalto buena)
	(personalidadRequerida Pedir-cita responsable)
	(accion Pedir-cita cita Neutral Sobresalto mala)
	(personalidadRequerida Pedir-cita responsable)


	; Primer turno
	(turno juan token)

	(= (puntos) 0)
	(= (puntosMoral buena) 1)
	(= (puntosMoral regular) 3)
	(= (puntosMoral mala) 5)
	(= (ronda) 0)
	(= (minimoRondas cita ) 1)
)

(:goal (and
	(escenarioTerminado cita)
))

(:metric minimize (puntos))

)	