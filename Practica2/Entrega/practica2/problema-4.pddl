(define (problem four)
(:domain TALKING)
(:objects
	cita - escenario
	cita2 - escenario
	cita3 - escenario
	juan - bot
	isabel - bot 	
	Agresividad - emocion
	Satisfaccion - emocion
	Temor - emocion
	Confianza - emocion
	Paz_Interior - emocion
	Susto - emocion
	Neutral - emocion
	Tristeza - emocion
	Culpabilidad - emocion
	Compasion - emocion
	Alegria - emocion
	Ilusion - emocion
	Placer - emocion
	Amor - emocion
	Enamoramiento - emocion
	Pavor - emocion
	Desilusion - emocion
	Decepcion - emocion
	Indignacion - emocion
	Tranquilidad - emocion
	Agonia - emocion
	Sobresalto - emocion
	Ira- emocion
	abierta - rasgo
	responsable - rasgo
	extrovertida - rasgo
	amable - rasgo
	inestable - rasgo
	Pedir-cita - accion
	Pedir-cita2 - accion
	Pedir-cita3 - accion
	Aceptar - accion
	Invitar-al-cine - accion
	Aceptar-propuesta - accion
	Ofrecerse-a-pagar - accion	
	buena - moral
	regular - moral
	mala - moral
	token - token
)

(:init
	; bots
	(in juan cita)
	(in isabel cita)
	(is juan abierta)
	(is isabel abierta)
	; emociones iniciales
	(feel juan Neutral)
	(feel isabel Neutral)
	; acciones con los rasgos necesarios (recordar que los rasgos necesarios pueden ser varios y bastará con cumplir uno de ellos.
	(accion Pedir-cita cita Neutral Sobresalto regular)
	(personalidadRequerida Pedir-cita abierta)
	(accion Pedir-cita2 cita2 Neutral Sobresalto buena)
	(personalidadRequerida Pedir-cita2 abierta)
	(accion Pedir-cita3 cita3 Neutral Sobresalto mala)
	(personalidadRequerida Pedir-cita3 abierta)


	; Primer turno
	(turno juan token)

	(= (puntos) 0)
	(= (puntosMoral buena) 1)
	(= (puntosMoral regular) 3)
	(= (puntosMoral mala) 5)
	(= (ronda) 0)
	(= (minimoRondas cita ) 1)
	(= (minimoRondas cita2 ) 1)
	(= (minimoRondas cita3 ) 1)
);

(:goal (and
	(escenarioTerminado cita)
	(escenarioTerminado cita2)
	(escenarioTerminado cita3)
))

(:metric minimize (puntos))

)	