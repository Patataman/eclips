(define (problem two)
(:domain TALKING)
(:objects
	cita - escenario
	juan - bot
	isabel - bot 	
	Agresividad - emocion
	Satisfaccion - emocion
	Temor - emocion
	Confianza - emocion
	Paz_Interior - emocion
	Susto - emocion
	Neutral - emocion
	Tristeza - emocion
	Culpabilidad - emocion
	Compasion - emocion
	Alegria - emocion
	Ilusion - emocion
	Placer - emocion
	Amor - emocion
	Enamoramiento - emocion
	Pavor - emocion
	Desilusion - emocion
	Decepcion - emocion
	Indignacion - emocion
	Tranquilidad - emocion
	Agonia - emocion
	Sobresalto - emocion
	Ira- emocion
	abierta - rasgo
	responsable - rasgo
	extrovertida - rasgo
	amable - rasgo
	inestable - rasgo
	Pedir-citaM - accion
	Pedir-citaB - accion
	AceptarR - accion
	AceptarB - accion
	buena - moral
	regular - moral
	mala - moral
	token - token
)

(:init
	; bots
	(in juan cita)
	(in isabel cita)
	(is juan abierta)
	(is isabel abierta)
	(is juan responsable)
	(is isabel responsable)
	; emociones iniciales
	(feel juan Neutral)
	(feel isabel Neutral)
	; acciones con los rasgos necesarios (recordar que los rasgos necesarios pueden ser varios y bastará con cumplir uno de ellos.
	(accion Pedir-citaM cita Neutral Sobresalto mala)
	(personalidadRequerida Pedir-citaM abierta)
	(personalidadRequerida Pedir-citaM extrovertida)
	(accion Pedir-citaB cita Neutral Sobresalto buena)
	(personalidadRequerida Pedir-citaB amable)
	(accion AceptarR cita Sobresalto Ilusion regular)
	(personalidadRequerida AceptarR responsable)
	(accion AceptarB cita Sobresalto Ilusion buena)
	(personalidadRequerida AceptarB abierta)



	; Primer turno
	(turno juan token)

	(= (puntos) 0)
	(= (puntosMoral buena) 1)
	(= (puntosMoral regular) 3)
	(= (puntosMoral mala) 5)
	(= (ronda) 0)
	(= (minimoRondas cita ) 2)
)

(:goal (and
	(escenarioTerminado cita)
))

(:metric minimize (puntos))

)	