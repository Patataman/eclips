(define (domain TALKING)
  (:requirements :strips :typing :fluents :equality) ;Se define como problema básico y que se puede definir tipos
	(:types 
		escenario  	; Se definen los tipos para escenario
		bot 		; bot (jugadores)
		emocion 	; emoción, porque se gestionarán emociones
		rasgo		; Rasgo de la personalidad que se puede poseer
		accion 		; acción ya que los bots realizarán acciones que generan emociones
		moral 		; Calificación de la gestion
		token)		; Token del turno
		

	(:predicates 
		(in ?x - bot ?y - escenario) 	; Un bot está en un escenario
		(is ?x - bot ?y - rasgo)		; Un bot tiene rasgos asignados como personalidad
		(feel ?x - bot ?y - emocion)	; Un bot siente una emocion
		(accion ?x - accion 			; Accion que se puede hacer
			?x2 - escenario 			; Escenario en el que se realiz la accion
			?y - emocion 				; Emoción que gestiona
			?w - emocion 				; Emoción que produce
			?z - moral) 				; Si la gestion es buena, regular o mala
		(personalidadRequerida ?x - accion ?y - rasgo) 	; Para ejecutar una accion se requiere uno o más rasgos
		(turno ?x - bot ?y - token)						; Un bot tiene el token si le toca jugar
		(escenarioTerminado ?x - escenario)				; El objetivo es jugar en todos los escenarios, se debe marcar aquellos terminados.
		)

	(:functions
		(puntos)					; Puntos acumulados, necesitamos ir calculado la calidad de la solución.
		(puntosMoral ?moral)		; función para obtener los puntos de una determinada moral.  1 -> Buena, 3 -> Regular, 5 -> Mala
		(ronda)						; Rondas jugadas
		(minimoRondas ?escenario)	; Rondas que se van a jugar en cada escenario
		)

			  												
	(:action gestionar
		:parameters (?bot1 - bot 
			?bot2 - bot
			?escenario - escenario
			?emocionGestionada - emocion
			?emocionProducida - emocion
			?emocionOriginal - emocion
			?accion - accion
			?moral - moral
			?rasgo - rasgo
			?token - token
			)
		:precondition (and 
			(in ?bot1 ?escenario)
			(is ?bot1 ?rasgo)
			(in ?bot2 ?escenario)
			(not (= ?bot1 ?bot2))
			(feel ?bot1 ?emocionGestionada)
			(feel ?bot2 ?emocionOriginal)
			(turno ?bot1 ?token)
			(accion ?accion ?escenario ?emocionGestionada ?emocionProducida ?moral)
			(personalidadRequerida ?accion ?rasgo)
			)
		:effect
			(and 
			(not (turno ?bot1 ?token))
			(turno ?bot2 ?token)
			(not (feel ?bot2 ?emocionOriginal))
			(feel ?bot2 ?emocionProducida)
			(increase (puntos) (puntosMoral ?moral))
			(increase (ronda) 1)
			)
		)	  												
		
	(:action FinalizarEscenario	; Cuando se llegue a x rondas se finaliza el escenario.	
		:parameters (?bot1 - bot 
			?bot2 - bot
			?escenario - escenario
			)
		:precondition (and 
			(in ?bot1 ?escenario)
			(in ?bot2 ?escenario)
			(not (= ?bot1 ?bot2))
			(= (ronda) (minimoRondas ?escenario)) ; Ya se han superado las rondas de ese escenario
			)
		:effect
			(and
			(escenarioTerminado ?escenario) ; Se marca el escenario como terminado
			)
		)

	(:action cambioEscenario	; si quedan escenarios se cambia el escenario.	
		:parameters (?bot1 - bot 
			?bot2 - bot
			?emocion1 - emocion
			?emocion2 - emocion
			?escenarioTerminado - escenario
			?nuevoEscenario - escenario
			)
		:precondition (and
			(in ?bot1 ?escenarioTerminado)
			(in ?bot2 ?escenarioTerminado)
			(feel ?bot1 ?emocion1)
			(feel ?bot2 ?emocion2)
			(escenarioTerminado ?escenarioTerminado)
			(not (escenarioTerminado ?nuevoEscenario))
			(not (= ?bot1 ?bot2))
			)
		:effect
			(and
			(not (in ?bot1 ?escenarioTerminado))
			(not (in ?bot2 ?escenarioTerminado))
			(in ?bot1 ?nuevoEscenario); Los bots pasan al nuevo escenario
			(in ?bot2 ?nuevoEscenario)
			(not (feel ?bot1 ?emocion1))    ; Reseteamos los bots
			(not (feel ?bot2 ?emocion2))
			(feel ?bot1 Neutral)
			(feel ?bot2 Neutral)
			(decrease (ronda) (ronda))      ; reseteamos las rondas
			)
		)
)
