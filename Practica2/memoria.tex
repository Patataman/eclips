\documentclass[10pt,a4paper,titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[official]{eurosym}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage[table]{xcolor}
\usepackage{geometry}
\geometry{top=2.54cm, left=3.1cm, right=3.1cm, bottom=2.54cm}
\setlength{\parskip}{0.5em}
\usepackage{hyperref}
\usepackage{fancyref}
\usepackage{float}
\usepackage{lastpage} %número total de páginas
\usepackage{fancyhdr}

\hypersetup{
	colorlinks,
	citecolor=black,
	filecolor=black,
	linkcolor=black,
	urlcolor=black
}

\pagestyle{fancy}
\fancyhf{}
\rhead{UC3M}
\lhead{Ingeniería del Conocimiento}
\rfoot{Pág. \thepage \hspace{1pt} de \pageref{LastPage}}

\title{ \textbf{ \Huge{Práctica 2: Planificación Automática}}}
\author{
\def\arraystretch{1.5}
		\begin{tabular}{lr}
			\multicolumn{1}{l}{Grupo 83} \\ \cline{1-1} \\
			Daniel Alejandro Rodríguez López & 100316890 \\
			Adrián Borja Pimentel & 100315536 \\
		\end{tabular}
}

\begin{document}
\maketitle
\setcounter{page}{2}
\tableofcontents
\newpage

\setcounter{secnumdepth}{0}
\section{Introducción}
	En el siguiente documento se expondrán todas las cuestiones solicitadas en el enunciado en relación a la práctica realizada, para ello se dedicará una sección a explicar el dominio detalladamente, el cual se encuentra en el fichero llamado \textit{domain.pddl}, otra sección para las seis pruebas diseñadas con sus respectivos resultados y conclusiones, y por último una sección de conclusiones generales sobre la práctica.

\setcounter{secnumdepth}{3}
\section{Manual técnico}
	En esta sección se explicará el dominio creado, definiendo los tipos declarados, predicados y acciones posibles. Para explicar mejor las diferentes partes del dominio, se dedicará un apartado a cada una de ellas, explicando brevemente que elementos las componen y su finalidad.

	\subsection{types}
		Los objetos que componen el dominio son:

		\begin{itemize}
			\item \texttt{escenario}: Los escenarios sobre los que desarrollarán el juego.
			\item \texttt{bot}: Los jugadores automáticos que participarán en el juego.
			\item \texttt{emocion}: La emoción son aquello que los bots sienten y que gestionan mediante acciones.
			\item \texttt{rasgo}: Cada uno de los rasgos de la personalidad según el modelo de ``Los Cinco Grandes''.
			\item \texttt{accion}: Las acciones son aquello que realizan los bots para gestionar sus emociones y avanzar en el juego. 
			\item \texttt{moral}: La moral es una calificación de una gestión. Puede ser buena, mala o regular.
			\item \texttt{token}: Este objeto se utiliza en el juego para saber a que bot le toca jugar.
		\end{itemize}

	\subsection{predicates}
		Los predicados que se han utilizado para definir los estados y las relaciones que pueden darse entre los objetos en el dominio son:
		\begin{itemize}
			\item \texttt{in}: Representa que un bot está en un escenario concreto.
			\item \texttt{is}: Representa los rasgos de la personalidad que posee un bot.
			\item \texttt{feel}: Representa la emoción que siente un bot.
			\item \texttt{accion}: Representa una acción que un bot puede realizar en un escenario concreto para gestionar una emoción y producir otra en el otro bot. La acción puede ser buena, mala o regular.
			\item \texttt{personalidadRequerida}: Representa los rasgos de la personalidad necesarios para ejecutar una acción. En esta versión del juego, el bot debe poseer uno de los rasgos de las personalidad que se requieren para realizar la acción, de lo contrario no podrá ejecutarla.
			\item \texttt{turno}: Representa que un bot tiene el token.
			\item \texttt{escenarioTerminado}: Representa que en un escenario ya se han realizado todas las rondas establecidas.
		\end{itemize}

	\subsection{functions}
		Las funciones se han utilizado principalmente para aplicar control sobre las acciones y poder aplicar una métrica con la que optimizar el plan. La métrica utilizada será minimizar los puntos obtenidos.

		\begin{itemize}
			\item \texttt{puntosMoral}: Puntos que genera cierta gestión de la moral. Por ejemplo, una moral ``Buena'' genera 1 punto.
			\item \texttt{puntos}: Puntos acumulados por la gestión de la moral. Una moral buena genera 1 punto, moral mala 5 puntos y moral regular 3 puntos.
			\item \texttt{ronda}: Número de rondas jugadas hasta el momento en el escenario actual. Comienza en 0 y se reinicia cada vez que se cambia de escenario.
			\item \texttt{minimoRondas}: Indica el número mínimo de rondas que se van a jugar por escenario.
		\end{itemize}

	\subsection{action gestionar}
		Esta acción gestiona las acciones que puede realizarle un bot a otro, verificando que la acción se pueda realizar y aplicando los cambios correspondientes, como es: cambiar la emoción sentida por el otro bot, los puntos acumulados y el turno.

		\paragraph{parámetros}
			Los parámetros necesarios son:
			\begin{itemize}
				\item Los dos bots que forman parte del juego.
				\item El escenario en el que se están desarrollando las acciones.
				\item La emoción que se va a gestionar por el primer bot.
				\item La emoción que se va a producir en el segundo bot.
				\item La emoción que sentía el segundo bot antes de la acción.
				\item La acción que se realizaría.
				\item La moral de la acción.
				\item El rasgo de la personalidad necesario para ejecutar la acción.
				\item El token.
			\end{itemize}
		\paragraph{precondiciones}
			En las precondiciones se verifica:
			\begin{itemize}
				\item Tanto el Bot 1 como el Bot 2 son distintos y se encuentran en el mismo escenario.
				\item Bot 1 posee la personalidad recuperada.
				\item Bot 1 posee la emoción que se va a gestionar (\texttt{?emocionGestionada}) y Bot 2 posee la emoción inicial (\texttt{?emocionOriginal}).
				\item El turno actual es de Bot 1.
				\item La acción a realizar debe ser una de las posibles para el escenario, y debe poder gestionar y generar las emociones de cada bot. Por último, debe estar asociada a la personalidad del Bot 1.
			\end{itemize}
		\paragraph{efectos}
			Los efectos que causa esta acción son:
			\begin{itemize}
				\item Cambiar el turno de juego, de Bot 1 a Bot 2.
				\item Cambiar la emoción que siente Bot 2 por la emoción que genera la acción.
				\item Incrementar los puntos dependiendo de la moral de la acción.
				\item Aumentar el número de rondas en 1.
			\end{itemize}		

	\subsection{action FinalizarEscenario}
		Un escenario finaliza cuando se alcanza el número mínimo de rondas definidas para el mismo.
		\paragraph{parámetros}
			Los parámetros recibidos se corresponden con los dos jugadores (Bot 1 y Bot 2) y el escenario actual.

		\paragraph{precondiciones}
			Las comprobaciones realizadas son:
			\begin{itemize}
				\item Bot 1 y Bot 2 se encuentran en el escenario actual.
				\item Bot 1 y Bot 2 no son el mismo.
				\item Se ha alcanzado el número mínimo de rondas a jugar para el escenario actual.
			\end{itemize}

		\paragraph{efectos}
			El efecto que causa esta acción consiste en indicar el escenario actual como finalizado.

	\subsection{action cambioEscenario}
		Una vez finalizado un escenario es necesario continuar en caso de que existiesen más escenarios.

		\paragraph{parámetros}
			Los parámetros necesarios son Bot 1, Bot 2, el escenario que acaba de finalizar y el nuevo escenario que se quiere iniciar, y las emociones actuales de los bots.

		\paragraph{precondiciones}
			Las comprobaciones realizadas son:
			\begin{itemize}
				\item El escenario recuperado como finalizado debe estar declarado como finalizado.
				\item Bot 1 y Bot 2 se encuentren en el escenario que acaba de finalizar.
				\item Bot 1 y Bot 2 son distintos.
				\item El \texttt{nuevoEscenario} no es un escenario finalizado.
				\item Las emociones escogidas son las que sienten actualmente los bots.
			\end{itemize}

		\paragraph{efectos}
			El efecto que causa esta acción es comenzar un nuevo escenario. Por lo tanto se deben realizar las siguientes acciones:
			\begin{itemize}
				\item ``Eliminar'' Bot 1 y Bot 2 del escenario actual.
				\item ``Añadir'' Bot 1 y Bot 2 en el nuevo escenario.
				\item Cambiar sus emociones actuales a la emoción \texttt{Neutral}.
				\item Reiniciar el número de rondas realizadas en el escenario.
			\end{itemize}

			
\section{Pruebas realizadas}
	Las pruebas se analizarán individualmente, comentando los planes obtenidos en cada situación. Para cada prueba se expondrá el plan obtenido, los puntos acumulados y el tiempo total de ejecución.

	Como recordatorio, el objetivo es minimizar los puntos y las acciones tienen los formatos:
	\begin{itemize}
		\item gestionar $<$bot1$>$ $<$bot2$>$ $<$escenario$>$ $<$emocionGestionada$>$ $<$emocionGenerada$>$ $<$EmocionOriginal$>$ $<$accion$>$ $<$moral$>$ $<$rasgoPersonalidad$>$ $<$token$>$
		\item finalizarEscenario $<$bot1$>$ $<$bot2$>$ $<$escenario$>$.
		\item cambioEscenario $<$bot1$>$ $<$bot2$>$ $<$emocionBot1$>$ $<$emocionBot2$>$ $<$escenarioAntiguo$>$ $<$escenarioNuevo$>$
	\end{itemize}
	
	\subsection{Prueba 1}
		La primera prueba consiste en un escenario con una única acción posible pero con distintos niveles de moralidad y distintos rasgos de la personalidad asociados. Se debería ejecutar la mejor acción que se pueda ejecutar.

		Ambos experimentos funcionan correctamente al realizar la acción con el rasgo de la personalidad adecuado, y además se observa que en el experimento con -O la acción que escoge es la de moral buena.

		\subsubsection{Plan obtenido sin -O}
				\begin{tabular}{l}
				\texttt{Puntos: 3} \\
				\texttt{Tiempo: 0.48} \\
				\texttt{0: GESTIONAR JUAN ISABEL CITA NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA REGULAR ABIERTA TOKEN} \\
				\texttt{1: FINALIZARESCENARIO ISABEL JUAN CITA} \\
			\end{tabular}

		\subsubsection{Plan obtenido con -O}

			\begin{tabular}{l}
				\texttt{Puntos: 1} \\
				\texttt{Tiempo: 0.43} \\
				\texttt{0: GESTIONAR JUAN ISABEL CITA NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA BUENA ABIERTA TOKEN} \\
		        \texttt{1: FINALIZARESCENARIO ISABEL JUAN CITA} \\
			\end{tabular}



	\subsection{Prueba 2}
		En esta prueba se ha profundizado en los rasgos de la personalidad requeridos para ejecutar las acciones con el fin de verificar que las acciones que se escogen son aquellas para las que los bots cumplen los rasgos requeridos.

		El comportamiento es el esperado, y aunque existen acciones con mejor moral, en la prueba optimizada no las escoge porque no se cumplen los requisitos de personalidad.

		\subsubsection{Plan obtenido sin -O}
			\begin{tabular}{l}
				\texttt{Puntos: 8} \\
				\texttt{Tiempo: 0.55} \\
				\texttt{0: GESTIONAR JUAN ISABEL CITA NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITAM MALA ABIERTA TOKEN} \\
				\texttt{1: GESTIONAR ISABEL JUAN CITA SOBRESALTO ILUSION NEUTRAL ACEPTARR REGULAR RESPONSABLE TOKEN} \\
				\texttt{2: FINALIZARESCENARIO ISABEL JUAN CITA} \\
			\end{tabular}
		\subsubsection{Plan obtenido con -O}
			\begin{tabular}{l}
				\texttt{Puntos: 6} \\
				\texttt{Tiempo: 0.55} \\
				\texttt{0: GESTIONAR JUAN ISABEL CITA NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITAM MALA ABIERTA TOKEN} \\
				\texttt{1: GESTIONAR ISABEL JUAN CITA SOBRESALTO ILUSION NEUTRAL ACEPTARB BUENA ABIERTA TOKEN} \\
				\texttt{2: FINALIZARESCENARIO ISABEL JUAN CITA} \\
			\end{tabular}

	\subsection{Prueba 3}
		Observando que la prueba anterior funciona correctamente, vamos a realizar otra prueba en la que en los bots no cumplen los rasgos necesarios para realizar las acciones suficientes como para que el escenario termine, por lo que en caso de funcionar correctamente, no se debería encontrar un plan válido.

		El resultado es el esperado, ambas pruebas tardaron 0.62 segundos.

	\subsection{Prueba 4}
		En esta prueba se verificará que al realizar el número de acciones establecidas para cada escenario, este finalizará y se cambiará a otro. Habrá 3 escenarios, y es necesario realizar una acción en cada uno para poder finalizarlos.

		El resultado es el esperado.

		\subsubsection{Plan obtenido sin -O}
			\begin{tabular}{l}
				\texttt{Puntos: 9} \\
				\texttt{Tiempo: 0.99} \\
				\texttt{0: GESTIONAR JUAN ISABEL CITA NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA REGULAR ABIERTA TOKEN} \\
				\texttt{1: FINALIZARESCENARIO ISABEL JUAN CITA} \\
				\texttt{2: CAMBIOESCENARIO ISABEL JUAN SOBRESALTO NEUTRAL CITA CITA2} \\
				\texttt{3: GESTIONAR ISABEL JUAN CITA2 NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA2 BUENA ABIERTA TOKEN} \\
				\texttt{4: FINALIZARESCENARIO ISABEL JUAN CITA2} \\
				\texttt{5: CAMBIOESCENARIO ISABEL JUAN NEUTRAL SOBRESALTO CITA2 CITA3} \\
				\texttt{6: GESTIONAR JUAN ISABEL CITA3 NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA3 MALA ABIERTA TOKEN} \\
				\texttt{7: FINALIZARESCENARIO ISABEL JUAN CITA3} \\
			\end{tabular}
		\subsubsection{Plan obtenido con -O}
			\begin{tabular}{l}
				\texttt{Puntos: 9} \\
				\texttt{Tiempo: 0.98} \\
				\texttt{0: GESTIONAR JUAN ISABEL CITA NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA REGULAR ABIERTA TOKEN} \\
				\texttt{1: FINALIZARESCENARIO ISABEL JUAN CITA} \\
				\texttt{2: CAMBIOESCENARIO ISABEL JUAN SOBRESALTO NEUTRAL CITA CITA3} \\
				\texttt{3: GESTIONAR ISABEL JUAN CITA3 NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA3 MALA ABIERTA TOKEN} \\
				\texttt{4: FINALIZARESCENARIO ISABEL JUAN CITA3} \\
				\texttt{5: CAMBIOESCENARIO ISABEL JUAN NEUTRAL SOBRESALTO CITA3 CITA2} \\
				\texttt{6: GESTIONAR JUAN ISABEL CITA2 NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA2 BUENA ABIERTA TOKEN} \\
				\texttt{7: FINALIZARESCENARIO ISABEL JUAN CITA2} \\

			\end{tabular}

	\subsection{Prueba 5}
		Como prueba final, se ha definido un juego completo con varios escenarios en los que se deben realizar varias acciones para completarlos.

		La ejecución ha sido exitosa y se aprecia como la ejecución optimizada obtiene mejor puntuación. Para ejecutar este problema ha habido problemas técnicos, siendo imposible ejecutarlos en nuestros ordenadores personales, en Guernika ejecuta sin problemas.

		\subsubsection{Plan obtenido sin -O}
			\begin{tabular}{l}
				\texttt{Puntos: 38} \\
				\texttt{Tiempo: 2.26} \\
				\texttt{0: GESTIONAR JUAN ISABEL CITA NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA REGULAR ABIERTA TOKEN} \\
				\texttt{1: GESTIONAR ISABEL JUAN CITA SOBRESALTO ILUSION NEUTRAL ACEPTAR BUENA ABIERTA TOKEN} \\
				\texttt{2: GESTIONAR JUAN ISABEL CITA ILUSION ENAMORAMIENTO SOBRESALTO INVITAR-AL-CINE BUENA ABIERTA TOKEN} \\
				\texttt{3: GESTIONAR ISABEL JUAN CITA ENAMORAMIENTO ALEGRIA ILUSION ACEPTAR-PROPUESTA BUENA ABIERTA TOKEN} \\
				\texttt{4: GESTIONAR JUAN ISABEL CITA ALEGRIA AMOR ENAMORAMIENTO OFRECERSE-A-PAGAR BUENA ABIERTA TOKEN} \\
				\texttt{5: FINALIZARESCENARIO ISABEL JUAN CITA} \\
				\texttt{6: CAMBIOESCENARIO ISABEL JUAN AMOR ALEGRIA CITA ENFADO} \\
				\texttt{7: GESTIONAR ISABEL JUAN ENFADO NEUTRAL TEMOR NEUTRAL CASTIGAR MALA ABIERTA TOKEN} \\
				\texttt{8: GESTIONAR JUAN ISABEL ENFADO TEMOR SUSTO NEUTRAL IRSE-DE-CASA MALA ABIERTA TOKEN} \\
				\texttt{9: GESTIONAR ISABEL JUAN ENFADO SUSTO AGRESIVIDAD TEMOR GRITAR MALA ABIERTA TOKEN} \\
				\texttt{10: FINALIZARESCENARIO ISABEL JUAN ENFADO} \\
				\texttt{11: CAMBIOESCENARIO ISABEL JUAN SUSTO AGRESIVIDAD ENFADO SPOILER} \\
				\texttt{12: GESTIONAR JUAN ISABEL SPOILER NEUTRAL AGRESIVIDAD NEUTRAL SPOILER MALA ABIERTA TOKEN} \\
				\texttt{13: GESTIONAR ISABEL JUAN SPOILER AGRESIVIDAD TRISTEZA NEUTRAL DEVOLVER-SPOILER MALA ABIERTA TOKEN} \\
				\texttt{14: GESTIONAR JUAN ISABEL SPOILER TRISTEZA CULPABILIDAD AGRESIVIDAD DECIR-QUE-ERA-MENTIRA REGULAR ABIERTA TOKEN} \\
				\texttt{15: GESTIONAR ISABEL JUAN SPOILER CULPABILIDAD CONFIANZA TRISTEZA INVITAR-A-CENAR REGULAR ABIERTA TOKEN} \\
				\texttt{16: FINALIZARESCENARIO ISABEL JUAN SPOILER} \\
			\end{tabular}

		\subsubsection{Plan obtenido con -O}
			\begin{tabular}{l}
				\texttt{Puntos: 34} \\
				\texttt{Tiempo: 2.25} \\
				\texttt{0: GESTIONAR JUAN ISABEL CITA NEUTRAL SOBRESALTO NEUTRAL PEDIR-CITA REGULAR ABIERTA TOKEN} \\
				\texttt{1: GESTIONAR ISABEL JUAN CITA SOBRESALTO ILUSION NEUTRAL ACEPTAR BUENA ABIERTA TOKEN} \\
				\texttt{2: GESTIONAR JUAN ISABEL CITA ILUSION ENAMORAMIENTO SOBRESALTO INVITAR-AL-CINE BUENA ABIERTA TOKEN} \\
				\texttt{3: GESTIONAR ISABEL JUAN CITA ENAMORAMIENTO ALEGRIA ILUSION ACEPTAR-PROPUESTA BUENA ABIERTA TOKEN} \\
				\texttt{4: GESTIONAR JUAN ISABEL CITA ALEGRIA AMOR ENAMORAMIENTO OFRECERSE-A-PAGAR BUENA ABIERTA TOKEN} \\
				\texttt{5: FINALIZARESCENARIO ISABEL JUAN CITA} \\
				\texttt{6: CAMBIOESCENARIO ISABEL JUAN AMOR ALEGRIA CITA SPOILER} \\
				\texttt{7: GESTIONAR ISABEL JUAN SPOILER NEUTRAL AGRESIVIDAD NEUTRAL SPOILER MALA ABIERTA TOKEN} \\
				\texttt{8: GESTIONAR JUAN ISABEL SPOILER AGRESIVIDAD TRISTEZA NEUTRAL DEVOLVER-SPOILER MALA ABIERTA TOKEN} \\
				\texttt{9: GESTIONAR ISABEL JUAN SPOILER TRISTEZA CULPABILIDAD AGRESIVIDAD DECIR-QUE-ERA-MENTIRA REGULAR ABIERTA TOKEN} \\
				\texttt{10: GESTIONAR JUAN ISABEL SPOILER CULPABILIDAD CONFIANZA TRISTEZA INVITAR-A-CENAR REGULAR ABIERTA TOKEN} \\
				\texttt{11: FINALIZARESCENARIO ISABEL JUAN SPOILER} \\
				\texttt{12: CAMBIOESCENARIO ISABEL JUAN CONFIANZA CULPABILIDAD SPOILER ENFADO} \\
				\texttt{13: GESTIONAR ISABEL JUAN ENFADO NEUTRAL TEMOR NEUTRAL CASTIGAR MALA ABIERTA TOKEN} \\
				\texttt{14: GESTIONAR JUAN ISABEL ENFADO TEMOR SUSTO NEUTRAL IRSE-DE-CASA MALA ABIERTA TOKEN} \\
				\texttt{15: GESTIONAR ISABEL JUAN ENFADO SUSTO CONFIANZA TEMOR RAZONAR BUENA ABIERTA TOKEN} \\
				\texttt{16: FINALIZARESCENARIO ISABEL JUAN ENFADO} \\
			\end{tabular}

	\subsection{Prueba 6}
		Como una prueba adicional, hemos comprobado como se comportaría el dominio en caso de existir 3 bots en un único escenario. El comportamiento es adecuado, teniendo la particularidad de que entra en una secuencia cíclica debida a la poca complejidad del problema, en la ejecución optimizada esta secuencia es la que mejor puntuación proporciona, comportamiento que esperábamos y que sirve para verificar el correcto funcionamiento del dominio.

		\subsubsection{Plan obtenido sin -O}
			\begin{tabular}{l}
				\texttt{Puntos: 57} \\
				\texttt{Tiempo: 1.10} \\
				\texttt{0: GESTIONAR JUAN TINO DEBATE NEUTRAL AGRESIVIDAD NEUTRAL DECIR-ALGO-MALO MALA ABIERTA TOKEN} \\
				\texttt{1: GESTIONAR TINO JUAN DEBATE AGRESIVIDAD AGRESIVIDAD NEUTRAL DECIR-ALGO-MALOAA MALA INESTABLE TOKEN} \\
				\texttt{2: GESTIONAR JUAN ISABEL DEBATE AGRESIVIDAD CONFIANZA NEUTRAL DAR-RAZONA MALA ABIERTA TOKEN} \\
				\texttt{3: GESTIONAR ISABEL TINO DEBATE CONFIANZA INDIGNACION AGRESIVIDAD DECIR-ALGO-MALOCI MALA RESPONSABLE TOKEN} \\
				\texttt{4: GESTIONAR TINO ISABEL DEBATE INDIGNACION CONFIANZA CONFIANZA EXPRESAR-OPINION BUENA INESTABLE TOKEN} \\
				\texttt{5: GESTIONAR ISABEL TINO DEBATE CONFIANZA AGRESIVIDAD INDIGNACION DECIR-ALGO-MALOCA MALA RESPONSABLE TOKEN} \\
				\texttt{6: GESTIONAR TINO JUAN DEBATE AGRESIVIDAD AGRESIVIDAD AGRESIVIDAD DECIR-ALGO-MALOAA MALA INESTABLE TOKEN} \\
				\texttt{7: GESTIONAR JUAN TINO DEBATE AGRESIVIDAD CONFIANZA AGRESIVIDAD DAR-RAZONA MALA ABIERTA TOKEN} \\
				\texttt{8: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION CONFIANZA EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{9: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{10: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{11: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{12: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{13: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{14: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{15: FINALIZARESCENARIO TINO ISABEL DEBATE} \\
			\end{tabular}
			
		\subsubsection{Plan obtenido con -O}
			\begin{tabular}{l}
				\texttt{Puntos: 47} \\
				\texttt{Tiempo: 1.10} \\
				\texttt{0: GESTIONAR JUAN TINO DEBATE NEUTRAL AGRESIVIDAD NEUTRAL DECIR-ALGO-MALO MALA ABIERTA TOKEN} \\
				\texttt{1: GESTIONAR TINO JUAN DEBATE AGRESIVIDAD AGRESIVIDAD NEUTRAL DECIR-ALGO-MALOAA MALA INESTABLE TOKEN} \\
				\texttt{2: GESTIONAR JUAN TINO DEBATE AGRESIVIDAD CONFIANZA AGRESIVIDAD DECIR-ALGO-BUENO BUENA ABIERTA TOKEN} \\
				\texttt{3: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION NEUTRAL EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{4: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{5: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{6: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{7: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{8: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{9: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{10: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{11: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{12: GESTIONAR ISABEL TINO DEBATE INDIGNACION CONFIANZA CONFIANZA DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{13: GESTIONAR TINO ISABEL DEBATE CONFIANZA INDIGNACION INDIGNACION EXPRESAR-OPINION REGULAR INESTABLE TOKEN} \\
				\texttt{14: GESTIONAR ISABEL JUAN DEBATE INDIGNACION CONFIANZA AGRESIVIDAD DAR-RAZONI REGULAR RESPONSABLE TOKEN} \\
				\texttt{15: FINALIZARESCENARIO TINO ISABEL DEBATE} \\
			\end{tabular}

\section{Conclusiones}
	La conclusión más importante que hemos sacado de esta práctica es que la correcta definición de un dominio permite resolver todos los problemas de ese tipo simplemente añadiendo la información necesaria, es decir, la generalidad del dominio es fundamental para que con una única implementación de este se puedan representar todas las situaciones posibles y obtener un plan para resolverlas. Además, al mostrar Metric-FF el plan obtenido permite un fácil análisis de la ejecución, e incluso se nos ocurre que con esa salida sería trivial generar las ordenes a ejecutar por las máquinas, trabajadores o entes que se encargan de realizar las acciones del dominio, pudiendo así por ejemplo organizar una fábrica, un equipo de trabajo o similares. Y si se usa la opción de optimizar y se ha definido una buena métrica, el plan generado será optimo.

	Hemos observado que en las pruebas la diferencia de tiempo entre las que tenían el parámetro -O y las que no, apenas variaban unas centésimas de segundo, por lo que quizá nuestros problemas no eran especialmente complejos, sin embargo el objetivo era evaluar el correcto funcionamiento del programa. El único problema detectado era que en el \textit{problema-5} ha sido imposible ejecutarlo en ordenadores personales, aún usando el comando \textit{ulimit}.

	En comparación con la práctica uno, esta nos ha resultado más sencilla, quizá porque ya conocíamos el problema y teníamos más claro que hacer. Definir el dominio nos ha parecido más natural a la forma de pensar que la ontología, aunque a la hora de definir el problema dudamos de cual ha sido más pesado, ya que en la ontología la personalidad era parte de la acción y en el dominio había que añadir una predicado para vincularlos, en esta practica no se ha profundizado mucho en las personalidades, pero de haberlo hecho, definir los problemas habría sido más costoso.

\end{document}