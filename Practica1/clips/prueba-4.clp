(definstances init
        ; Escenarios
        (of escenario (nombre "Enfado"))
        (of escenario (nombre "Spoiler"))
        (of escenario (nombre "Cita"))

        ; Emociones
        (of emocion (nombre Agresividad))
        (of emocion (nombre Satisfaccion))
        (of emocion (nombre Temor))
        (of emocion (nombre Confianza))
        (of emocion (nombre Paz_Interior))
        (of emocion (nombre Susto))
        (of emocion (nombre Neutral))
        (of emocion (nombre Tristeza))
        (of emocion (nombre Culpabilidad))
        (of emocion (nombre Compasion))
        (of emocion (nombre Alegria))
        (of emocion (nombre Ilusion))
        (of emocion (nombre Placer))
        (of emocion (nombre Amor))
        (of emocion (nombre Enamoramiento))
        (of emocion (nombre Pavor))
        (of emocion (nombre Desilusion))
        (of emocion (nombre Decepcion))
        (of emocion (nombre Indignacion))
        (of emocion (nombre Tranquilidad))
        (of emocion (nombre Agonia))
        (of emocion (nombre Sobresalto))
        (of emocion (nombre Ira))


        ;Personalidades
        (of personalidad (nombre Amable)
                                  (abierta 60)
                                  (responsable 80)
                                  (extrovertida 60)
                                  (amable 100)
                                  (inestable 0))
        (of personalidad (nombre Bipolar)
                                  (abierta 40)
                                  (responsable 30)
                                  (extrovertida 60)
                                  (amable 80)
                                  (inestable 80))
        (of personalidad (nombre Borde)
                                  (abierta 0)
                                  (responsable 50)
                                  (extrovertida 10)
                                  (amable 0)
                                  (inestable 60))
        (of personalidad (nombre Plasta)
                                  (abierta 100)
                                  (responsable 20)
                                  (extrovertida 100)
                                  (amable 50)
                                  (inestable 50))
        (of personalidad (nombre Random)
                                  (abierta 100)
                                  (responsable 100)
                                  (extrovertida 100)
                                  (amable 100)
                                  (inestable 100))


        ; Acciones
        (of accion (descripcion Castigar)
                                (escenario "Enfado")    ; Escenario que afecta
                                (emocion Neutral)               ; Que emocion se gestiona
                                (moral Mala)            ; Si la acción es buena o mala gestión
                                (emocionGenerada Temor) ; Emoción que genera

                                (abierta 0)
                                (responsable 0)
                                (extrovertida 0)
                                (amable 0)
                                (inestable 0))

        ; Spoiler
        (of accion (descripcion Spoiler)
                                (escenario "Spoiler")   ; Escenario que afecta
                                (emocion Neutral)               ; Que emocion se gestiona
                                (moral Mala)            ; Si la acción es buena o mala gestión
                                (emocionGenerada Agresividad) ; Emoción que genera

                                (abierta 0)
                                (responsable 0)
                                (extrovertida 0)
                                (amable 0)
                                (inestable 0))

        ; Cita
        (of accion (descripcion Pedir-cita)
                                (escenario "Cita")      ; Escenario que afecta
                                (emocion Neutral)               ; Que emocion se gestiona
                                (moral Regular)         ; Si la acción es buena o mala gestión
                                (emocionGenerada Sobresalto) ; Emoción que genera

                                (abierta 0)
                                (responsable 0)
                                (extrovertida 0)
                                (amable 0)
                                (inestable 0))

)

(deffacts init
        (flag-escenario (boolean 0))
)