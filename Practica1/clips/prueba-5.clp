(definstances init
	; Escenarios
	(of escenario (nombre "Enfado"))
	(of escenario (nombre "Spoiler"))
	(of escenario (nombre "Cita"))

	; Emociones
	(of emocion (nombre Agresividad))
	(of emocion (nombre Satisfaccion))
	(of emocion (nombre Temor))
	(of emocion (nombre Confianza))
	(of emocion (nombre Paz_Interior))
	(of emocion (nombre Susto))
	(of emocion (nombre Neutral))
	(of emocion (nombre Tristeza))
	(of emocion (nombre Culpabilidad))
	(of emocion (nombre Compasion))
	(of emocion (nombre Alegria))
	(of emocion (nombre Ilusion))
	(of emocion (nombre Placer))
	(of emocion (nombre Amor))
	(of emocion (nombre Enamoramiento))
	(of emocion (nombre Pavor))
	(of emocion (nombre Desilusion))
	(of emocion (nombre Decepcion))
	(of emocion (nombre Indignacion))
	(of emocion (nombre Tranquilidad))
	(of emocion (nombre Agonia))
	(of emocion (nombre Sobresalto))
	(of emocion (nombre Ira))


	;Personalidades
	(of personalidad (nombre Amable)
				  (abierta 60)
				  (responsable 80)
				  (extrovertida 60)
				  (amable 100)
				  (inestable 0))
	(of personalidad (nombre Bipolar)
				  (abierta 40)
				  (responsable 30)
				  (extrovertida 60)
				  (amable 80)
				  (inestable 80))
	(of personalidad (nombre Borde)
				  (abierta 0)
				  (responsable 50)
				  (extrovertida 10)
				  (amable 0)
				  (inestable 60))
	(of personalidad (nombre Plasta)
				  (abierta 100)
				  (responsable 20)
				  (extrovertida 100)
				  (amable 50)
				  (inestable 50))
	(of personalidad (nombre Random)
				  (abierta 100)
				  (responsable 100)
				  (extrovertida 100)
				  (amable 100)
				  (inestable 100))


	; Acciones
	(of accion (descripcion Castigar)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Neutral)		; Que emocion se gestiona
				(moral Mala)		; Si la acción es buena o mala gestión
				(emocionGenerada Temor) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 0)
				(amable 0)
				(inestable 0))

	(of accion (descripcion Obedecer)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Temor)		; Que emocion se gestiona
				(moral Buena)		; Si la acción es buena o mala gestión
				(emocionGenerada Satisfaccion) ; Emoción que genera

				(abierta 10)
				(responsable 60)
				(extrovertida 10)
				(amable 10)
				(inestable 10))

	(of accion (descripcion Irse-de-casa)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Temor)		; Que emocion se gestiona
				(moral Mala)		; Si la acción es buena o mala gestión
				(emocionGenerada Susto) ; Emoción que genera

				(abierta 30)
				(responsable 0)
				(extrovertida 10)
				(amable 0)
				(inestable 70))

	(of accion (descripcion Escuchar-chapa)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Temor)		; Que emocion se gestiona
				(moral Regular)		; Si la acción es buena o mala gestión
				(emocionGenerada Paz_Interior) ; Emoción que genera

				(abierta 30)
				(responsable 80)
				(extrovertida 10)
				(amable 80)
				(inestable 0))

	(of accion (descripcion Razonar)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Susto)		; Que emocion se gestiona
				(moral Buena)		; Si la acción es buena o mala gestión
				(emocionGenerada Confianza) ; Emoción que genera

				(abierta 20)
				(responsable 60)
				(extrovertida 50)
				(amable 50)
				(inestable 0))

	(of accion (descripcion Gritar)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Susto)		; Que emocion se gestiona
				(moral Mala)		; Si la acción es buena o mala gestión
				(emocionGenerada Agresividad) ; Emoción que genera

				(abierta 20)
				(responsable 20)
				(extrovertida 10)
				(amable 10)
				(inestable 70))


	; Spoiler
	(of accion (descripcion Spoiler)
				(escenario "Spoiler")	; Escenario que afecta
				(emocion Neutral)		; Que emocion se gestiona
				(moral Mala)		; Si la acción es buena o mala gestión
				(emocionGenerada Agresividad) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 0)
				(amable 0)
				(inestable 0))

	(of accion (descripcion Devolver-spoiler)
				(escenario "Spoiler")	; Escenario que afecta
				(emocion Agresividad)		; Que emocion se gestiona
				(moral Mala)		; Si la acción es buena o mala gestión
				(emocionGenerada Tristeza) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 10)
				(amable 20)
				(inestable 50))

	(of accion (descripcion Perdonar)
				(escenario "Spoiler")	; Escenario que afecta
				(emocion Agresividad)		; Que emocion se gestiona
				(moral Buena)		; Si la acción es buena o mala gestión
				(emocionGenerada Culpabilidad) ; Emoción que genera

				(abierta 20)
				(responsable 10)
				(extrovertida 10)
				(amable 50)
				(inestable 0))

	(of accion (descripcion Decir-que-era-mentira)
				(escenario "Spoiler")	; Escenario que afecta
				(emocion Tristeza)		; Que emocion se gestiona
				(moral Regular)		; Si la acción es buena o mala gestión
				(emocionGenerada Culpabilidad) ; Emoción que genera

				(abierta 20)
				(responsable 0)
				(extrovertida 20)
				(amable 0)
				(inestable 30))

	(of accion (descripcion Invitar-a-cenar)
				(escenario "Spoiler")	; Escenario que afecta
				(emocion Culpabilidad)		; Que emocion se gestiona
				(moral Regular)		; Si la acción es buena o mala gestión
				(emocionGenerada Confianza) ; Emoción que genera

				(abierta 20)
				(responsable 0)
				(extrovertida 30)
				(amable 20)
				(inestable 0))

	(of accion (descripcion Gritar)
				(escenario "Spoiler")	; Escenario que afecta
				(emocion Culpabilidad)		; Que emocion se gestiona
				(moral Mala)		; Si la acción es buena o mala gestión
				(emocionGenerada Susto) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 10)
				(amable 0)
				(inestable 50))

	; Cita
	(of accion (descripcion Pedir-cita)
				(escenario "Cita")	; Escenario que afecta
				(emocion Neutral)		; Que emocion se gestiona
				(moral Regular)		; Si la acción es buena o mala gestión
				(emocionGenerada Sobresalto) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 0)
				(amable 0)
				(inestable 0))

	(of accion (descripcion Ignorar)
				(escenario "Cita")	; Escenario que afecta
				(emocion Sobresalto)		; Que emocion se gestiona
				(moral Regular)		; Si la acción es buena o mala gestión
				(emocionGenerada Desilusion) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 0)
				(amable 0)
				(inestable 50))

		(of accion (descripcion Mostrar-desilusion)
					(escenario "Cita")	; Escenario que afecta
					(emocion Desilusion)		; Que emocion se gestiona
					(moral Buena)		; Si la acción es buena o mala gestión
					(emocionGenerada Culpabilidad) ; Emoción que genera

					(abierta 20)
					(responsable 30)
					(extrovertida 0)
					(amable 20)
					(inestable 0))

		(of accion (descripcion Mostrar-que-te-da-igual)
					(escenario "Cita")	; Escenario que afecta
					(emocion Desilusion)		; Que emocion se gestiona
					(moral Regular)		; Si la acción es buena o mala gestión
					(emocionGenerada Indignacion) ; Emoción que genera

					(abierta 20)
					(responsable 0)
					(extrovertida 30)
					(amable 0)
					(inestable 0))

			(of accion (descripcion irse-indignado)
						(escenario "Cita")	; Escenario que afecta
						(emocion Indignacion)		; Que emocion se gestiona
						(moral Regular)		; Si la acción es buena o mala gestión
						(emocionGenerada Decepcion) ; Emoción que genera

						(abierta 0)
						(responsable 0)
						(extrovertida 0)
						(amable 0)
						(inestable 0))

			(of accion (descripcion Mostrar-indignacion)
						(escenario "Cita")	; Escenario que afecta
						(emocion Indignacion)		; Que emocion se gestiona
						(moral Buena)		; Si la acción es buena o mala gestión
						(emocionGenerada Culpabilidad) ; Emoción que genera

						(abierta 30)
						(responsable 0)
						(extrovertida 0)
						(amable 0)
						(inestable 10))

		(of accion (descripcion Insultar)
					(escenario "Cita")	; Escenario que afecta
					(emocion Desilusion)		; Que emocion se gestiona
					(moral Mala)		; Si la acción es buena o mala gestión
					(emocionGenerada Temor) ; Emoción que genera

					(abierta 0)
					(responsable 0)
					(extrovertida 0)
					(amable 0)
					(inestable 50))

			(of accion (descripcion Salir-corriendo)
						(escenario "Cita")	; Escenario que afecta
						(emocion Temor)		; Que emocion se gestiona
						(moral Regular)		; Si la acción es buena o mala gestión
						(emocionGenerada Ira) ; Emoción que genera

						(abierta 10)
						(responsable 0)
						(extrovertida 0)
						(amable 0)
						(inestable 30))

				(of accion (descripcion Perseguir)
							(escenario "Cita")	; Escenario que afecta
							(emocion Ira)		; Que emocion se gestiona
							(moral Mala)		; Si la acción es buena o mala gestión
							(emocionGenerada Pavor) ; Emoción que genera

							(abierta 0)
							(responsable 0)
							(extrovertida 0)
							(amable 0)
							(inestable 50))

					(of accion (descripcion Seguir-corriendo)
								(escenario "Cita")	; Escenario que afecta
								(emocion Pavor)		; Que emocion se gestiona
								(moral Buena)		; Si la acción es buena o mala gestión
								(emocionGenerada Ira) ; Emoción que genera

								(abierta 0)
								(responsable 20)
								(extrovertida 0)
								(amable 0)
								(inestable 0))

						(of accion (descripcion Dejar-escapar)
									(escenario "Cita")	; Escenario que afecta
									(emocion Ira)		; Que emocion se gestiona
									(moral Buena)		; Si la acción es buena o mala gestión
									(emocionGenerada Tranquilidad) ; Emoción que genera

									(abierta 0)
									(responsable 30)
									(extrovertida 0)
									(amable 20)
									(inestable 0))

					(of accion (descripcion Suplicar-perdon)
								(escenario "Cita")	; Escenario que afecta
								(emocion Pavor)		; Que emocion se gestiona
								(moral Regular)		; Si la acción es buena o mala gestión
								(emocionGenerada Culpabilidad) ; Emoción que genera

								(abierta 60)
								(responsable 0)
								(extrovertida 0)
								(amable 0)
								(inestable 0))

						(of accion (descripcion pedir-perdon)
									(escenario "Cita")	; Escenario que afecta
									(emocion Culpabilidad)		; Que emocion se gestiona
									(moral Buena)		; Si la acción es buena o mala gestión
									(emocionGenerada Tranquilidad) ; Emoción que genera

									(abierta 0)
									(responsable 30)
									(extrovertida 0)
									(amable 0)
									(inestable 0))

							(of accion (descripcion Sonreir-e-irse)
										(escenario "Cita")	; Escenario que afecta
										(emocion Tranquilidad)		; Que emocion se gestiona
										(moral Buena)		; Si la acción es buena o mala gestión
										(emocionGenerada Tranquilidad) ; Emoción que genera

										(abierta 0)
										(responsable 0)
										(extrovertida 30)
										(amable 0)
										(inestable 0))

						(of accion (descripcion Golpear-e-irse)
									(escenario "Cita")	; Escenario que afecta
									(emocion Culpabilidad)		; Que emocion se gestiona
									(moral Mala)		; Si la acción es buena o mala gestión
									(emocionGenerada Agonia) ; Emoción que genera

									(abierta 0)
									(responsable 0)
									(extrovertida 0)
									(amable 0)
									(inestable 70))

						(of accion (descripcion irse-sin-mas)
									(escenario "Cita")	; Escenario que afecta
									(emocion Culpabilidad)		; Que emocion se gestiona
									(moral Mala)		; Si la acción es buena o mala gestión
									(emocionGenerada Tranquilidad) ; Emoción que genera

									(abierta 0)
									(responsable 20)
									(extrovertida 0)
									(amable 0)
									(inestable 0))



	(of accion (descripcion Aceptar)
				(escenario "Cita")	; Escenario que afecta
				(emocion Sobresalto)		; Que emocion se gestiona
				(moral Buena)		; Si la acción es buena o mala gestión
				(emocionGenerada Ilusion) ; Emoción que genera

				(abierta 20)
				(responsable 0)
				(extrovertida 30)
				(amable 20)
				(inestable 0))

		(of accion (descripcion Invitar-al-cine)
					(escenario "Cita")	; Escenario que afecta
					(emocion Ilusion)		; Que emocion se gestiona
					(moral Buena)		; Si la acción es buena o mala gestión
					(emocionGenerada Enamoramiento) ; Emoción que genera

					(abierta 20)
					(responsable 0)
					(extrovertida 20)
					(amable 20)
					(inestable 0))

		(of accion (descripcion Invitar-a-cenar)
					(escenario "Cita")	; Escenario que afecta
					(emocion Ilusion)		; Que emocion se gestiona
					(moral Buena)		; Si la acción es buena o mala gestión
					(emocionGenerada Enamoramiento) ; Emoción que genera

					(abierta 20)
					(responsable 0)
					(extrovertida 20)
					(amable 20)
					(inestable 0))

			(of accion (descripcion Aceptar-propuesta)
						(escenario "Cita")	; Escenario que afecta
						(emocion Enamoramiento)		; Que emocion se gestiona
						(moral Buena)		; Si la acción es buena o mala gestión
						(emocionGenerada Ilusion) ; Emoción que genera

						(abierta 30)
						(responsable 0)
						(extrovertida 0)
						(amable 10)
						(inestable 0))

			(of accion (descripcion Hacerse-el-duro)
						(escenario "Cita")	; Escenario que afecta
						(emocion Enamoramiento)		; Que emocion se gestiona
						(moral Mala)		; Si la acción es buena o mala gestión
						(emocionGenerada Indignacion) ; Emoción que genera

						(abierta 40)
						(responsable 0)
						(extrovertida 0)
						(amable 0)
						(inestable 20))

				(of accion (descripcion Seguir-la-corriente)
							(escenario "Cita")	; Escenario que afecta
							(emocion Indignacion)		; Que emocion se gestiona
							(moral Regular)		; Si la acción es buena o mala gestión
							(emocionGenerada Ilusion) ; Emoción que genera

							(abierta 20)
							(responsable 0)
							(extrovertida 30)
							(amable 10)
							(inestable 0))

			(of accion (descripcion Besar)
						(escenario "Cita")	; Escenario que afecta
						(emocion Enamoramiento)		; Que emocion se gestiona
						(moral Buena)		; Si la acción es buena o mala gestión
						(emocionGenerada Enamoramiento) ; Emoción que genera

						(abierta 20)
						(responsable 0)
						(extrovertida 20)
						(amable 0)
						(inestable 10))

	(of accion (descripcion Dar-largas)
				(escenario "Cita")	; Escenario que afecta
				(emocion Sobresalto)		; Que emocion se gestiona
				(moral Regular)		; Si la acción es buena o mala gestión
				(emocionGenerada Decepcion) ; Emoción que genera

				(abierta 20)
				(responsable 30)
				(extrovertida 0)
				(amable 0)
				(inestable 20))

		(of accion (descripcion Asumir-fracaso)
					(escenario "Cita")	; Escenario que afecta
					(emocion Decepcion)		; Que emocion se gestiona
					(moral Buena)		; Si la acción es buena o mala gestión
					(emocionGenerada Tranquilidad) ; Emoción que genera

					(abierta 30)
					(responsable 20)
					(extrovertida 0)
					(amable 0)
					(inestable 0))

		(of accion (descripcion Negar-fracaso)
					(escenario "Cita")	; Escenario que afecta
					(emocion Decepcion)		; Que emocion se gestiona
					(moral Mala)		; Si la acción es buena o mala gestión
					(emocionGenerada Temor) ; Emoción que genera

					(abierta 0)
					(responsable 0)
					(extrovertida 0)
					(amable 0)
					(inestable 40))


)

(deffacts init
	(flag-escenario (boolean 0))
)