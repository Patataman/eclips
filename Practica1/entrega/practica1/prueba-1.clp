(definstances init
	; Escenarios
	(of escenario (nombre "Cita"))

	; Emociones
	(of emocion (nombre Agresividad))
	(of emocion (nombre Satisfaccion))
	(of emocion (nombre Temor))
	(of emocion (nombre Confianza))
	(of emocion (nombre Paz_Interior))
	(of emocion (nombre Susto))
	(of emocion (nombre Neutral))
	(of emocion (nombre Tristeza))
	(of emocion (nombre Culpabilidad))
	(of emocion (nombre Compasion))
	(of emocion (nombre Alegria))
	(of emocion (nombre Ilusion))
	(of emocion (nombre Placer))
	(of emocion (nombre Amor))
	(of emocion (nombre Enamoramiento))
	(of emocion (nombre Pavor))
	(of emocion (nombre Desilusion))
	(of emocion (nombre Decepcion))
	(of emocion (nombre Indignacion))
	(of emocion (nombre Tranquilidad))
	(of emocion (nombre Agonia))
	(of emocion (nombre Sobresalto))
	(of emocion (nombre Ira))


	;Personalidades
	(of personalidad (nombre Random)
				  (abierta 100)
				  (responsable 100)
				  (extrovertida 100)
				  (amable 100)
				  (inestable 100))


	; Acciones
	; Cita
	(of accion (descripcion Pedir-cita)
				(escenario "Cita")	; Escenario que afecta
				(emocion Neutral)		; Que emocion se gestiona
				(moral Regular)		; Si la acción es buena o mala gestión
				(emocionGenerada Sobresalto) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 0)
				(amable 0)
				(inestable 0))

		(of accion (descripcion Aceptar)
					(escenario "Cita")	; Escenario que afecta
					(emocion Sobresalto)		; Que emocion se gestiona
					(moral Buena)		; Si la acción es buena o mala gestión
					(emocionGenerada Ilusion) ; Emoción que genera

					(abierta 0)
					(responsable 0)
					(extrovertida 0)
					(amable 0)
					(inestable 0))

			(of accion (descripcion Invitar-al-cine)
						(escenario "Cita")	; Escenario que afecta
						(emocion Ilusion)		; Que emocion se gestiona
						(moral Buena)		; Si la acción es buena o mala gestión
						(emocionGenerada Enamoramiento) ; Emoción que genera

						(abierta 0)
						(responsable 0)
						(extrovertida 0)
						(amable 0)
						(inestable 0))

				(of accion (descripcion Aceptar-propuesta)
							(escenario "Cita")	; Escenario que afecta
							(emocion Enamoramiento)		; Que emocion se gestiona
							(moral Buena)		; Si la acción es buena o mala gestión
							(emocionGenerada Alegria) ; Emoción que genera

							(abierta 0)
							(responsable 0)
							(extrovertida 0)
							(amable 0)
							(inestable 0))

					(of accion (descripcion Ofrecerse-a-pagar)
								(escenario "Cita")	; Escenario que afecta
								(emocion Alegria)		; Que emocion se gestiona
								(moral Buena)		; Si la acción es buena o mala gestión
								(emocionGenerada Amor) ; Emoción que genera

								(abierta 0)
								(responsable 0)
								(extrovertida 0)
								(amable 0)
								(inestable 0))
)

(deffacts init
	(flag-escenario (boolean 0))
)