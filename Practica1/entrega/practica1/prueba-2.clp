(definstances init
	; Escenarios
	(of escenario (nombre "Enfado"))

	; Emociones
	(of emocion (nombre Agresividad))
	(of emocion (nombre Satisfaccion))
	(of emocion (nombre Temor))
	(of emocion (nombre Confianza))
	(of emocion (nombre Paz_Interior))
	(of emocion (nombre Susto))
	(of emocion (nombre Neutral))
	(of emocion (nombre Tristeza))
	(of emocion (nombre Culpabilidad))
	(of emocion (nombre Compasion))
	(of emocion (nombre Alegria))
	(of emocion (nombre Ilusion))
	(of emocion (nombre Placer))
	(of emocion (nombre Amor))
	(of emocion (nombre Enamoramiento))
	(of emocion (nombre Pavor))
	(of emocion (nombre Desilusion))
	(of emocion (nombre Decepcion))
	(of emocion (nombre Indignacion))
	(of emocion (nombre Tranquilidad))
	(of emocion (nombre Agonia))
	(of emocion (nombre Sobresalto))
	(of emocion (nombre Ira))


	;Personalidades
	(of personalidad (nombre Bipolar)
				  (abierta 40)
				  (responsable 30)
				  (extrovertida 60)
				  (amable 80)
				  (inestable 80))
	(of personalidad (nombre Random)
				  (abierta 100)
				  (responsable 100)
				  (extrovertida 100)
				  (amable 100)
				  (inestable 100))


	; Acciones
	(of accion (descripcion Castigar)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Neutral)		; Que emocion se gestiona
				(moral Mala)     		; Si la acción es buena o mala gestión
				(emocionGenerada Temor) ; Emoción que genera

				(abierta 40)
				(responsable 30)
				(extrovertida 60)
				(amable 80)
				(inestable 80))

	(of accion (descripcion Abrazar)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Neutral)		; Que emocion se gestiona
				(moral Bien)     		; Si la acción es buena o mala gestión
				(emocionGenerada Amor) ; Emoción que genera

				(abierta 300)
				(responsable 0)
				(extrovertida 0)
				(amable 0)
				(inestable 0))

	(of accion (descripcion Obedecer)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Temor)		; Que emocion se gestiona
				(moral Buena)		; Si la acción es buena o mala gestión
				(emocionGenerada Satisfaccion) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 0)
				(amable 0)
				(inestable 120))

	(of accion (descripcion Irse-de-casa)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Temor)		; Que emocion se gestiona
				(moral Mala)		; Si la acción es buena o mala gestión
				(emocionGenerada Susto) ; Emoción que genera

				(abierta 40)
				(responsable 30)
				(extrovertida 60)
				(amable 80)
				(inestable 80))

	(of accion (descripcion Escuchar-chapa)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Temor)		; Que emocion se gestiona
				(moral Regular)		; Si la acción es buena o mala gestión
				(emocionGenerada Paz_Interior) ; Emoción que genera

				(abierta 0)
				(responsable 0)
				(extrovertida 200)
				(amable 0)
				(inestable 0))

	(of accion (descripcion Razonar)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Susto)		; Que emocion se gestiona
				(moral Buena)		; Si la acción es buena o mala gestión
				(emocionGenerada Confianza) ; Emoción que genera

				(abierta 40)
				(responsable 30)
				(extrovertida 60)
				(amable 80)
				(inestable 80))

	(of accion (descripcion Gritar)
				(escenario "Enfado")	; Escenario que afecta
				(emocion Susto)		; Que emocion se gestiona
				(moral Mala)		; Si la acción es buena o mala gestión
				(emocionGenerada Agresividad) ; Emoción que genera

				(abierta 0)
				(responsable 300)
				(extrovertida 0)
				(amable 0)
				(inestable 0))
)

(deffacts init
	(flag-escenario (boolean 0))
)