(defclass emocion (is-a INITIAL-OBJECT)
	(slot nombre)
)

;los cinco rasgos o factores principales se suelen denominar tradicionalmente como: factor O (Openness o apertura a nuevas experiencias), factor C (Conscientiousness o responsabilidad), factor E (Extraversion o extroversión), factor A (Agreeableness o amabilidad) y factor N (Neuroticism o inestabilidad emocional),
(defclass individuo (is-a INITIAL-OBJECT)
	(slot nombre)
	(slot personalidad);Nombre del objeto personalidad que tiene
	(slot emocion (default Neutral))
)

(defclass user (is-a individuo))

(defclass bot (is-a individuo))

(defclass personalidad (is-a INITIAL-OBJECT)
	(slot nombre)
	(slot abierta (type INTEGER))
	(slot responsable (type INTEGER))
	(slot extrovertida (type INTEGER))
	(slot amable (type INTEGER))
	(slot inestable (type INTEGER))
)

(defclass accion (is-a INITIAL-OBJECT)
	(slot escenario)	; Escenario que afecta
	(slot emocion)		; Que emocion se gestiona
	(slot moral)		; Si la acción es buena o mala gestión
	(slot descripcion)
	(slot emocionGenerada) ; Emoción que genera
	;(slot personalidad)	; Que tipo de personalidad asociada a esta gestion
	(slot abierta (type INTEGER))
	(slot responsable (type INTEGER))
	(slot extrovertida (type INTEGER))
	(slot amable (type INTEGER))
	(slot inestable (type INTEGER))
)

(defclass escenario (is-a INITIAL-OBJECT)
	(slot nombre)
)

(defclass escenario-escogido (is-a escenario))

(deftemplate turno
	(slot nombre)
	(slot jugada (type INTEGER))
)

(deftemplate flag-escenario
	(slot boolean)
)

;(send [gen10] print)